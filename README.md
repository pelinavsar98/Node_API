## DevOps Case Study
##### Pelin Avsar - pelinavsar98@gmail.com
---

Hello!

I will try to explain what I have done in this project as a whole experience. 

I didn't have any experience with Kubernetes, or CI/CD pipelines. So I might have done embarrassing noob stuff. 

But I believe I learned much and did some cool stuff too!

### CI phase

For the CI, I used GitLab. I created an image from the source code provided and pushed the image to Dockerhub. 

Whenever there's a change to the app source code, [the image](https://hub.docker.com/repository/docker/plnvsr/nodeapp/general) gets updated automatically. This saves a lot of time, and reduces errors that might occur. 

First I thought building a custom postgres image would be handy too. I already had an init.sql file, so I could use this to create the database and populate it. I tried doing it that way first by moving the init.sql file

```
COPY init.sql ./docker-entrypoint-initdb.d/
```

It didn't work the way intended. I tried different things such as pulling the base image, creating a docker container so I can run init.sql then push the image. With the latest method, when I pull the image and ran docker run on my local, I could see the database and the table. But when I deploy it on K8S, K8S somehow overrides.

I realized doing it this way would create the database storage inside the image anyways. Plus, it made the build pipeline much slower, because I had to wait in order for the container to be created, so I could execute init.sql. Instead, I decided to use the base image for postgres, and adjusted changes on K8S manifests.

### What is Kubernetes phase

I installed necessities:

* Minikube
* Helm 

After hours of self education, I wrote the following manifests:

* Deployment & service for NodeJS app 
* Deployment & service for Postgres
* Secret for Postgres
* Config for Postgres
* PVC for Postgres data

I tried to write the templates in a way such that nearly all values are referenced from Values.yaml file. This way, the app can be deployed on different environments easily. 

I designed having two branches for this project:

* **Master** (default branch) that would be used to deploy on **production** environment
* **Dev** for **development** environment

Merge requests to Master branch are approved by hand. So the code in Dev can't automatically promote to Master branch. 

### 💀💀💀 phase

After doing everything, I wanted to test the app on the NodePort service and it didn't work. I inspected everything on kubectl, and everything seemed ok. I copy pasted my yaml files to ChatGPT many times. I thought it had something to do with the database connection, so I double checked everything again and again. To see if that was the issue I added a simple route for / that just says Hello, and that even didn't work! 

I spent hours trying to debug it. After, I found out, there's a way to test the pods from the inside. I created this pod: 

```
apiVersion: v1
kind: Pod
metadata:
  name: debug-pod
spec:
  containers:
  - name: debug-container
    image: busybox 
    command: ["/bin/sh"]
    args: ["-c", "sleep infinity"]
```

and ran wget in it, with cluster IPs. Apparently, my code was working and it was a minikube win11 (?) [issue](https://stackoverflow.com/questions/71536310/unable-to-access-minikube-ip-address). 😊😊😊 I would be glad to hear if my NodePort service works on your system!



### Setting up ArgoCD

Now that everything is _not_ over, it's time for CD. While trying to debug I ran helm package, helm install, helm upgrade, helm uninstall dozens of times so I understand why CD is very important. I followed the [documentation](https://argo-cd.readthedocs.io/en/stable/getting_started/) and added the app. 


#### Working with different environments easily 

Instead of giving the values such as --path or --repo by hand, I created ArgoCD manifests. Here is the manifest to deploy to Development environment: 

```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: nodejs-dev
spec:
  project: default
  source:
    repoURL: https://gitlab.com/pelinavsar98/Node_API.git
    targetRevision: dev
    path: helm-chart
    helm:
      valueFiles:
        - values-dev.yaml
  destination:
    server: https://kubernetes.default.svc 
    namespace: default  
```

On your local, you can do: 

```
argocd app create nodejs-dev --file application-dev.yaml
```

That will create the _nodejs-dev_ app and sync it with the dev branch. 

![Alt text](readmeimages/image.png)

To test the functionality, I created two different image repos on DockerHub. As we can observe in the app, the correct image gets used. 

![Alt text](readmeimages/image-1.png)

Let's try the same for the master branch for Production environment. 

```
argocd app create nodejs-prod --file application-prod.yaml
```

![Alt text](readmeimages/image-2.png)

Let's check the NodeJS container.

![Alt text](readmeimages/image-3.png)

Works as intended. I guess that's the end of the journey. 

Thanks for reading!